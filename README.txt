CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Conflicts/Known issues
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------


Elevator.js fixes those awkward "scroll to top" moments the old fashioned way. This module add this feature to your Drupal website


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/elevator

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/elevator




REQUIREMENTS
------------

This module requires no modules outside of Drupal core.




CONFLICTS/KNOWN ISSUES
----------------------




INSTALLATION
------------

 * Install the Elevator module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.



CONFIGURATION
------------

 * A few value are set in the standard yml file. At the current stage, you can update those via code




MAINTAINER
----------

   * Augusto Fagioli (afagioli) - https://www.drupal.org/u/afagioli




Supporting organizations:

 * Fagioli.biz - https://fagioli.biz




Thanks to  https://freesound.org for CC Licensed demo sounds
