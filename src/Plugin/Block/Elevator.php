<?php

namespace Drupal\elevator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Elevator' Block.
 *
 * @Block(
 *   id = "elevator_block",
 *   admin_label = @Translation("Elevator block"),
 *   category = @Translation("Elevator"),
 * )
 */
class Elevator extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output  = "<div class=\"elevator-button\">" .  $this->t('Scroll up') . "</div>";
    return [
      '#markup' => $output,
    ];
  }

}
