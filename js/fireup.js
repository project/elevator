window.onload = function() {
   console.log("this is elevator working");
   new Elevator({
       element: document.querySelector('.elevator-button'),
       mainAudio: 'mp3/main.mp3',
       endAudio: 'mp3/end.mp3',
       duration: 5000,
       startCallback: function() {
         // is called, when the elevator starts moving
       },
       endCallback: function() {
         // is called, when the elevator reached target level
       }
   });
}
